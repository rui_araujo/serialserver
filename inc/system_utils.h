/*
 * system_utils.h
 *
 *  Created on: May 20, 2014
 *      Author: raraujo
 */

#ifndef SYSTEM_UTILS_H_
#define SYSTEM_UTILS_H_
#include <stdint.h>

/**
 * Open a serial port with the name passed as an argument
 * @param serPortName Serial Port name
 * @param[in|out] Requested baudrate, actual baudrate returned.
 * @return 0 if there are no errors
 */
int serPortOpen(char *serPortName, int *baudrate);

/**
 * Prints the IP address and the port of the incoming connection
 * @param connectionFd Socket file descriptor
 */
void printIncomingConnection(int connectionFd);

/**
 * A blocking write function for a file descriptor
 * @param fd File descriptor
 * @param buf Buffer with the data
 * @param size Size of the buffer
 * @return 0 if there are no errors
 */
int blockingWrite(int fd, unsigned char * buf, int size);

/**
 *
 * @return
 */
uint64_t us_since_epoch();
#endif /* SYSTEM_UTILS_H_ */
