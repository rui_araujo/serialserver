/*
 * system_utils.c
 *
 *  Created on: May 20, 2014
 *      Author: raraujo
 */
#include "system_utils.h"
#include <arpa/inet.h>
#include <termios.h>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/socket.h>
#include <sys/time.h>
#include <fcntl.h>
#include <errno.h>
#include <stdio.h>
#include <stdint.h>
#include <linux/serial.h>
#include <termio.h>

static speed_t getBaudrate(int baudrate) {
	switch (baudrate) {
	case 50:
		return B50;
	case 75:
		return B75;
	case 110:
		return B110;
	case 134:
		return B134;
	case 150:
		return B150;
	case 200:
		return B200;
	case 300:
		return B300;
	case 600:
		return B600;
	case 1200:
		return B1200;
	case 1800:
		return B1800;
	case 2400:
		return B2400;
	case 4800:
		return B4800;
	case 9600:
		return B9600;
	case 19200:
		return B19200;
	case 38400:
		return B38400;
	case 57600:
		return B57600;
	case 115200:
		return B115200;
	case 230400:
		return B230400;
	case 460800:
		return B460800;
	case 500000:
		return B500000;
	case 576000:
		return B576000;
	case 921600:
		return B921600;
	case 1000000:
		return B1000000;
	case 1152000:
		return B1152000;
	case 1500000:
		return B1500000;
	case 2000000:
		return B2000000;
	case 2500000:
		return B2500000;
	case 3000000:
		return B3000000;
	case 3500000:
		return B3500000;
	case 4000000:
		return B4000000;
	}
	return 0;
}

int serPortOpen(char *serPortName, int *baudrate) {
	int serPortFD;
	struct termios tty;
	struct serial_struct ser;

	if (!baudrate) {
		return -1;
	}
	if ((serPortFD = open(serPortName, O_RDWR)) < 0) {
		printf("can't open serial port file %s\n", serPortName);
		return (-1);
	}

	speed_t baudrateFlags = getBaudrate(*baudrate);
	if (baudrateFlags == 0) {
		baudrateFlags = B38400;
		if (ioctl(serPortFD, TIOCGSERIAL, &ser) < 0)
			return -1;
		ser.flags &= ~ASYNC_SPD_MASK;
		ser.flags |= ASYNC_SPD_CUST;
		ser.custom_divisor = ser.baud_base / *baudrate;
		if (ioctl(serPortFD, TIOCSSERIAL, &ser) < 0)
			return -1;
		if (ioctl(serPortFD, TIOCGSERIAL, &ser) < 0)
			return -1;
		if (*baudrate != (ser.baud_base / ser.custom_divisor)) {
			*baudrate = ser.baud_base / ser.custom_divisor;
			fprintf(stderr, "Failed to set requested baudrate.\n");
		}
	}

	tcgetattr(serPortFD, &tty); /* get record of parameters */
	//Hard coded to 4M because that's what it is used in all retina and robot
	cfsetospeed(&tty, baudrateFlags); /* output speed */
	cfsetispeed(&tty, baudrateFlags); /* input speed */

	/*
	 * The serial port is configured as non canonical mode, ie, raw mode.
	 */
	tty.c_iflag &= ~(IGNBRK | BRKINT | PARMRK | ISTRIP | INLCR | IGNCR | ICRNL | IXON);
	tty.c_oflag &= ~OPOST;
	tty.c_lflag &= ~(ECHO | ECHONL | ICANON | ISIG | IEXTEN);
	tty.c_cflag &= ~(CSIZE | PARENB);
	tty.c_cflag |= CS8;/* 8 bits */
	tty.c_cflag |= CLOCAL | CREAD;
	tty.c_cflag |= CRTSCTS;						// use hardware handshaking
	tty.c_cc[VMIN] = 0;
	tty.c_cc[VTIME] = 10;						// 1 second timeout
	/* write modified record of parameters to port */
	if (tcsetattr(serPortFD, TCSANOW, &tty)) {
		printf("serPort fatal: error during configuration!\n");
	}
	//Set the serial port to blocking mode
	if (fcntl(serPortFD, F_SETFL, 0)) {
		close(serPortFD);
		return -1;
	}
	return serPortFD;
}

void printIncomingConnection(int connectionFd) {
	socklen_t len;
	struct sockaddr_storage addr;
	char ipstr[INET6_ADDRSTRLEN];
	getpeername(connectionFd, (struct sockaddr*) &addr, &len);
	struct sockaddr_in *s = (struct sockaddr_in *) &addr;
	inet_ntop(AF_INET, &s->sin_addr, ipstr, sizeof ipstr);
	fprintf(stdout, "New connection from %s:%d\n", ipstr, ntohs(s->sin_port));
}

int blockingWrite(int fd, unsigned char * buf, int size) {
	int nwrite = 0;
	for (int i = 0; i < size; i += nwrite) {
		/* loop in time! */
		/* write might not take it all in one call,
		 * so we have to try until it's all written
		 */
		nwrite = write(fd, buf + i, size - i);
		if (nwrite < 0) {
			//Non blocking file descriptor support
			if (errno != EWOULDBLOCK) {
				return -1;
			}
		}
	}
	return 0;
}

uint64_t us_since_epoch() {
	struct timeval tv;
	uint64_t micros = 0;
	gettimeofday(&tv, NULL);
	micros = ((uint64_t) tv.tv_sec) * 1000000 + tv.tv_usec;
	return micros;
}
