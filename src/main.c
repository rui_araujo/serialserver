/*
 * main.c
 *
 *  Created on: May 19, 2014
 *      Author: raraujo
 */

#include <sys/socket.h>
#include <netinet/in.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <signal.h>
#include <fcntl.h>
#include <pthread.h>
#include <string.h>
#include <ctype.h>
#include <stdbool.h>
#include "system_utils.h"

#define BUFSIZE 					1024

#define DEFAULT_PORT 				56000

#define DEFAULT_BAUDRATE 			4000000
#define DEFAULT_SERIAL_PORTNAME		"/dev/ttyUSB0"

static volatile int closeServer = 0; //Outside loop variable
/*
 * Socket file descriptor
 * This is a global variable so that the termination handler can execute the
 * shutdown call on the socket to quickly exit the server.
 */
static volatile int socketFd = -1;

/**
 * Handler of Unix termination signals
 * @param param Unused
 */
void termination_handler(int param) {
	(void) param;
	fprintf(stdout, "Exiting\n");
	closeServer = 1;
	//Shutdown the socket fd so that the main thread can exit cleanly.
	if (socketFd != -1) {
		//We are exiting so we don't care about errors
		shutdown(socketFd, SHUT_RDWR);
	}
}

struct readerArg {
	int serialPortFd; //Serial port file descriptor
	bool serialError; //Flag that indicates there was an error with the serial port
	bool * stopRequested; //Pointer to the stop requesting flag
	int sockedFd; //Socket file descriptor
};

void * serialReader(void *arg) {
	unsigned char receptionBuf[BUFSIZE + 1];
	int nread;
	struct readerArg * tArg = arg;
	while (!(*tArg->stopRequested)) {
		nread = read(tArg->serialPortFd, receptionBuf, BUFSIZE);
		if (nread == -1) {
			fprintf(stderr, "Serial read failed\n");
			tArg->serialError = true;
			*tArg->stopRequested = true;
			break;
		} else if (nread > 0) {
#if DEBUG
			receptionBuf[nread] = '\0'; //safe because the buffer has one extra character
			printf("%s", receptionBuf);
			fflush(stdout);
#endif

			if (blockingWrite(tArg->sockedFd, receptionBuf, nread)) {
				fprintf(stderr, "Serial write failed\n");
				*tArg->stopRequested = true;
				break;
			}
		}
	}
	return arg;
}

int main(int argc, char *argv[]) {
	(void) argc;
	(void) argv;
	int serialPortFd;
	char * serialPortName;
	int baudrate = DEFAULT_BAUDRATE;

	if (argc > 1) {
		serialPortName = argv[1];
		if (argc > 2) {
			baudrate = atoi(argv[2]);
			if (baudrate <= 0) {
				baudrate = DEFAULT_BAUDRATE;
			}
		}
	} else {
		serialPortName = DEFAULT_SERIAL_PORTNAME;
	}

	/*threads ids which are used to stop for the threads to finish
	 * before accepting a new connection */
	pthread_t serialReaderTid;
	struct readerArg readerArgs; //arguments for the reader threads

	int connectionFd = 0; //the connection file descriptor
	struct sockaddr_in serv_addr;
	unsigned char receptionBuf[BUFSIZE + 1];
	int nread;
	bool socketClosed = false, stopRequested = false;
	/*This signal is useless
	 * It may occurs when the client disconnects suddenly.*/
	signal(SIGPIPE, SIG_IGN);
	struct sigaction new_action, old_action;

	/* Handle stopping signals */
	new_action.sa_handler = termination_handler;
	sigemptyset(&new_action.sa_mask);
	new_action.sa_flags = 0;

	//Respect the ignore settings that the process may have inherited
	sigaction(SIGINT, NULL, &old_action);
	if (old_action.sa_handler != SIG_IGN)
		sigaction(SIGINT, &new_action, NULL);
	sigaction(SIGHUP, NULL, &old_action);
	if (old_action.sa_handler != SIG_IGN)
		sigaction(SIGHUP, &new_action, NULL);
	sigaction(SIGTERM, NULL, &old_action);
	if (old_action.sa_handler != SIG_IGN)
		sigaction(SIGTERM, &new_action, NULL);

	/* The reader threads write to the socket directly
	 * so they share a mutex to protect the critical section*/
	readerArgs.serialPortFd = serialPortFd = serPortOpen(serialPortName, &baudrate);
	if (serialPortFd == -1) {
		fprintf(stderr, "%s is not responding.\nExitting\n", serialPortName);
		return -1; //Any failure to open the serial port at this stage equals termination.
	} else {
		printf("Opened %s with %d bps.\n", serialPortName, baudrate);
	}
	readerArgs.serialError = false;
	//Common stop requesting flag for all the threads
	readerArgs.stopRequested = &stopRequested;
	//Each writer gets its own input packet queue that this thread will write to

	//Socket Creation
	socketFd = socket(AF_INET, SOCK_STREAM, 0);
	if (socketFd < 0) {
		fprintf(stderr, "Error setting up the server\n");
		goto error;
		//hate this but it looks better
	}
	memset(&serv_addr, 0, sizeof(serv_addr));

	serv_addr.sin_family = AF_INET;
	serv_addr.sin_addr.s_addr = htonl(INADDR_ANY);
	serv_addr.sin_port = htons(DEFAULT_PORT);

	if (bind(socketFd, (struct sockaddr*) &serv_addr, sizeof(serv_addr))) {
		fprintf(stderr, "Error setting up the server\n");
		goto error;
		//hate this but it looks better
	}

	if (listen(socketFd, 10)) {
		fprintf(stderr, "Error setting up the server\n");
		goto error;
		//hate this but it looks better
	}

	/*
	 * This while loops ensure we accept a single client
	 * while handling disconnection smoothly
	 */
	while (!closeServer) {
		//It will block here until there is a connection or a termination signal
		connectionFd = accept(socketFd, (struct sockaddr*) NULL, NULL);
		if (connectionFd == -1) {
			if (!closeServer) {
				//No need to print this error message if caused by the shutdown call.
				fprintf(stderr, "Socket creation failed\n");
			}
			continue; //wait for another connection
		}
		//Prints the IP from the connection
		printIncomingConnection(connectionFd);

		//Reset stop variables
		stopRequested = false;
		socketClosed = false;
		//It updates the connection file descriptor and creates/starts the new thread
		readerArgs.sockedFd = connectionFd;
		pthread_create(&serialReaderTid, NULL, serialReader, &readerArgs);

		while (!socketClosed && !closeServer) {
			/*
			 * This call is blocked until 3 situations:
			 * 1) data nread > 0
			 * 2) clean exit nread =0
			 * 3) dirty exit nread =-1
			 */
			nread = read(connectionFd, receptionBuf, BUFSIZE);
			if (nread == -1) {
				if (!closeServer) {
					fprintf(stderr, "Socket read failed\n");
				}
				break;
			} else if (nread > 0) {
				if (blockingWrite(serialPortFd, receptionBuf, nread)) {
					fprintf(stderr, "Serial write failed\n");
					stopRequested = true; //stop thread
					goto error;
					//hate this but it looks better
					break;
				}
			} else {
				/*
				 * When the connection is closed, all worker threads are stopped.
				 * If we detect an error with the serial port, we will try to recover
				 * If it proves to be impossible, we terminate.
				 */
				socketClosed = true;
				stopRequested = true;
				fprintf(stdout, "Socket closed\n");
				pthread_join(serialReaderTid, NULL);
				if (readerArgs.serialError) {
					//trying to recover the serial port
					close(readerArgs.serialPortFd);
					readerArgs.serialPortFd = serPortOpen(serialPortName, &baudrate);
					if (readerArgs.serialPortFd == -1) {
						fprintf(stderr, "%s is not responding.\nExitting\n", serialPortName);
						goto error;
						//hate this but it looks better
					}
				}

				close(connectionFd);
			}
		}

	}
	error:
//Clean up time!
	close(connectionFd); //doesn't hurt
	close(socketFd);
	close(serialPortFd);

	return 0;
}
